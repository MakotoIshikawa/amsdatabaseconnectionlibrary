﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AmsDatabaseConnectionLibrary.Model.Common.Tests {
	[TestClass()]
	public class MonthlyInfoTests {
		[TestMethod()]
		[Owner(nameof(MonthlyInfo))]
		[TestCategory("生成")]
		[WorkItem(1)]
		public void MonthlyInfoTest() {
			{
				var m = new MonthlyInfo(2018, 4);

				Assert.AreEqual(2018, m.FiscalYear);
				Assert.AreEqual(4, m.FiscalMonth);

				Assert.AreEqual(2018, m.StartDay.Year);
				Assert.AreEqual(3, m.StartDay.Month);
				Assert.AreEqual(16, m.StartDay.Day);

				Assert.AreEqual(2018, m.EndDay.Year);
				Assert.AreEqual(4, m.EndDay.Month);
				Assert.AreEqual(15, m.EndDay.Day);

				Assert.AreEqual(m.StartDay, m.Days.First());
				Assert.AreEqual(m.EndDay, m.Days.Last());
			}
			{
				var m = new MonthlyInfo(2018, 1);

				Assert.AreEqual(2018, m.FiscalYear);
				Assert.AreEqual(1, m.FiscalMonth);

				Assert.AreEqual(2018, m.StartDay.Year);
				Assert.AreEqual(12, m.StartDay.Month);
				Assert.AreEqual(16, m.StartDay.Day);

				Assert.AreEqual(2019, m.EndDay.Year);
				Assert.AreEqual(1, m.EndDay.Month);
				Assert.AreEqual(15, m.EndDay.Day);

				Assert.AreEqual(m.StartDay, m.Days.First());
				Assert.AreEqual(m.EndDay, m.Days.Last());
			}
			{
				var m = new MonthlyInfo(2018, 3);

				Assert.AreEqual(2018, m.FiscalYear);
				Assert.AreEqual(3, m.FiscalMonth);

				Assert.AreEqual(2019, m.StartDay.Year);
				Assert.AreEqual(2, m.StartDay.Month);
				Assert.AreEqual(16, m.StartDay.Day);

				Assert.AreEqual(2019, m.EndDay.Year);
				Assert.AreEqual(3, m.EndDay.Month);
				Assert.AreEqual(15, m.EndDay.Day);

				Assert.AreEqual(m.StartDay, m.Days.First());
				Assert.AreEqual(m.EndDay, m.Days.Last());
			}
		}
	}
}