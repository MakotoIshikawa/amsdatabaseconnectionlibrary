﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AmsDatabaseConnectionLibrary.Model.Common.Tests {
	[TestClass()]
	public class FiscalYearMonthTests {
		[TestMethod()]
		[Owner(nameof(FiscalYearMonth))]
		[TestCategory("生成")]
		[WorkItem(1)]
		public void FiscalYearMonthTest() {
			{
				var fym = new FiscalYearMonth(2018, 4);

				Assert.AreEqual(2018, fym.FiscalYear);
				Assert.AreEqual(4, fym.FiscalMonth);
				Assert.AreEqual(2018, fym.RealYear);
			}
			{
				var fym = new FiscalYearMonth(2018, 1);

				Assert.AreEqual(2018, fym.FiscalYear);
				Assert.AreEqual(1, fym.FiscalMonth);
				Assert.AreEqual(2019, fym.RealYear);
			}
			{
				var fym = new FiscalYearMonth(2018, 3);

				Assert.AreEqual(2018, fym.FiscalYear);
				Assert.AreEqual(3, fym.FiscalMonth);
				Assert.AreEqual(2019, fym.RealYear);
			}
		}
	}
}