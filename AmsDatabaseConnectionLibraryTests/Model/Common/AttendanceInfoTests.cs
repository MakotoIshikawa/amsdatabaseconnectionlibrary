﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AmsDatabaseConnectionLibrary.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmsDatabaseConnectionLibrary.Model.Common.Tests {
	[TestClass()]
	public class AttendanceInfoTests {
		[TestMethod()]
		public void AttendanceInfoTest() {
			var at = new AttendanceInfo("8:00", "17:00");

			Assert.AreEqual(new TimeSpan(8, 0, 0), at.InTime);
			Assert.AreEqual(new TimeSpan(17, 0, 0), at.OutTime);
		}
	}
}