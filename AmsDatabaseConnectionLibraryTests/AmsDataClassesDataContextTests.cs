﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using AmsDatabaseConnectionLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AmsDatabaseConnectionLibrary.Tests {
	[TestClass()]
	public class AmsDataClassesDataContextTests {
		[TestMethod()]
		public void GetEnrolledUsersTest() {
			var sb = new StringBuilder();
			using (var db = new AmsDataClassesDataContext() { Log = new StringWriter(sb) }) {
				var users = db.GetEnrolledUsers().ToList();

				Assert.IsTrue(users.All(u => !u.del_date.HasValue));
			}
			Debug.WriteLine(sb);
		}

		[TestMethod()]
		public void GetEnrolledUserTest() {
			var sb = new StringBuilder();
			using (var db = new AmsDataClassesDataContext() { Log = new StringWriter(sb) }) {
				var user = db.GetEnrolledUser("0000000001");

				Assert.IsNotNull(user);
			}
			Debug.WriteLine(sb);
		}

		[TestMethod()]
		public void GetUserByCardTest() {
			var sb = new StringBuilder();
			using (var db = new AmsDataClassesDataContext() { Log = new StringWriter(sb) }) {
				var user = db.GetUserByCard("0000000000000001");

				Assert.IsNotNull(user);
			}
			Debug.WriteLine(sb);
		}

		[TestMethod()]
		public void DeleteAtLogicalOfUserTest() {
			var sb = new StringBuilder();
			using (var db = new AmsDataClassesDataContext() { Log = new StringWriter(sb) }) {
				db.DeleteAtLogicalOfUser("0000000001", "999999");

				Debug.WriteLine(sb);
				sb.Clear();

				var user = db.GetEnrolledUser("0000000001");
				Assert.IsNull(user);
			}
			Debug.WriteLine(sb);
		}

		[TestMethod()]
		public void RecoverOfUserTest() {
			var sb = new StringBuilder();
			using (var db = new AmsDataClassesDataContext() { Log = new StringWriter(sb) }) {
				db.RecoverOfUser("0000000001", "999999");

				Debug.WriteLine(sb);
				sb.Clear();

				var user = db.GetEnrolledUser("0000000001");
				Assert.IsNotNull(user);
			}
			Debug.WriteLine(sb);
		}

		[TestMethod]
		public void 月額取得() {
			var id = "00931498";
			var nendo = 2018;
			var getsudo = 4;
			var sb = new StringBuilder();
			using (var db = new AmsDataClassesDataContext("data source=SQL2014ENT;persist security info=False;initial catalog=AMS_YDM;User ID=sa;Password=admin;integrated security=false;") { Log = new StringWriter(sb) }) {
				var sum = db.GetMonthlyAmount(id, nendo, getsudo);

				Assert.IsNotNull(sum);
				Assert.AreEqual(6400m, sum.Value);
			}
			Debug.WriteLine(sb);
		}

		[TestMethod]
		public void ユーザー情報() {
			var user = new m_user {
				work_time = 7.5m
			};

			Assert.AreEqual(TimeSpan.FromHours(7.5), user.WorkTime);

			user.work_time = null;

			Assert.IsNull(user.WorkTime);
		}

		[TestMethod]
		[Owner(nameof(Nullable))]
		[TestCategory("変換")]
		[WorkItem(6)]
		public void Nullable文字列変換() {
			var nullableVal = new int?();
			{
				Assert.IsNull(nullableVal);
				Assert.IsFalse(nullableVal.HasValue);
				Assert.AreEqual(string.Empty, $"{nullableVal}");
				Assert.AreEqual(string.Empty, nullableVal.ToString());
				Assert.AreEqual(null, nullableVal?.ToString());

				var val = 99;
				nullableVal = val;

				Assert.IsNotNull(nullableVal);
				Assert.IsTrue(nullableVal.HasValue);
				Assert.AreEqual(val.ToString(), $"{nullableVal}");
				Assert.AreEqual(val.ToString(), nullableVal.ToString());
			}
		}
	}
}
