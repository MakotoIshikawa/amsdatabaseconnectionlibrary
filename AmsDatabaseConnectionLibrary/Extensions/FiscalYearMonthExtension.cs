﻿using AmsDatabaseConnectionLibrary.Interfaces;
using AmsDatabaseConnectionLibrary.Model.Common;

namespace AmsDatabaseConnectionLibrary.Extensions {
	/// <summary>
	/// IFiscalYearMonth を拡張するメソッドを提供します。
	/// </summary>
	public static partial class FiscalYearMonthExtension {
		#region メソッド

		/// <summary>
		/// 年月度の開始日と終了日の情報クラスに変換します。
		/// </summary>
		/// <param name="this">IFiscalYearMonth</param>
		/// <param name="firstDay">月の初日を設定します。</param>
		/// <param name="isPreviousMonth">月初日が前月の場合は true それ以外は false を設定します。</param>
		/// <returns>年月度の開始日と終了日の情報クラスの新しいインスタンスを返します。</returns>
		public static MonthlyInfo ToMonthlyInfo(IFiscalYearMonth @this, int firstDay = 16, bool isPreviousMonth = true)
			=> new MonthlyInfo(@this, firstDay, isPreviousMonth);

		#endregion
	}
}
