﻿using ExtensionsLibrary.Extensions;

namespace AmsDatabaseConnectionLibrary.Model.Primitive {
	/// <summary>
	/// モデルの基礎となる抽象クラスです。
	/// </summary>
	public abstract class ModelBase {
		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}
}

