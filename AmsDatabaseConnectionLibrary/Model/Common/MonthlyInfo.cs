﻿using System;
using System.Collections.Generic;
using AmsDatabaseConnectionLibrary.Interfaces;
using AmsDatabaseConnectionLibrary.Model.Primitive;

namespace AmsDatabaseConnectionLibrary.Model.Common {
	/// <summary>
	/// 年月度の開始日と終了日の情報を格納するモデルクラスです。
	/// </summary>
	public class MonthlyInfo : ModelBase, IFiscalYearMonth {
		#region コンストラクタ

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="fiscalYear">年度(4 月開始)を設定します。</param>
		/// <param name="fiscalMonth">月度を設定します。</param>
		/// <param name="firstDay">月の初日を設定します。</param>
		/// <param name="isPreviousMonth">月初日が前月の場合は true それ以外は false を設定します。</param>
		public MonthlyInfo(int fiscalYear, int fiscalMonth, int firstDay = 16, bool isPreviousMonth = true)
			: this(new FiscalYearMonth(fiscalYear, fiscalMonth), firstDay, isPreviousMonth) {
		}

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="fiscalYearMonth">年月度を設定します。</param>
		/// <param name="firstDay">月の初日を設定します。</param>
		/// <param name="isPreviousMonth">月初日が前月の場合は true それ以外は false を設定します。</param>
		public MonthlyInfo(IFiscalYearMonth fiscalYearMonth, int firstDay = 16, bool isPreviousMonth = true) {
			this.FiscalYearMonth = fiscalYearMonth;
			this.FirstDayOfMonth = firstDay;
			this.IsPreviousMonth = isPreviousMonth;
			this.Convert();
		}

		#endregion

		#region プロパティ

		/// <summary>
		/// 年月度
		/// </summary>
		public IFiscalYearMonth FiscalYearMonth { get; protected set; }

		/// <summary>
		/// 開始日
		/// </summary>
		public DateTime StartDay { get; protected set; }

		/// <summary>
		/// 終了日
		/// </summary>
		public DateTime EndDay { get; protected set; }

		/// <summary>
		/// 月の初日
		/// </summary>
		public int FirstDayOfMonth { get; protected set; }

		/// <summary>
		/// 月初日が前月の場合は true それ以外は false を返します。
		/// </summary>
		public bool IsPreviousMonth { get; protected set; }

		/// <summary>
		/// 開始日から終了日までの日付データのコレクションを取得します。
		/// </summary>
		public IEnumerable<DateTime> Days {
			get {
				var day = this.StartDay;
				while (day <= this.EndDay) {
					yield return day;

					day = day.AddDays(1);
				}
			}
		}

		/// <summary>
		/// 年度を取得します。
		/// </summary>
		public int FiscalYear => this.FiscalYearMonth.FiscalYear;

		/// <summary>
		/// 月度を取得します。
		/// </summary>
		public int FiscalMonth => this.FiscalYearMonth.FiscalMonth;

		/// <summary>
		/// 実際の年を取得します。
		/// </summary>
		public int RealYear => this.FiscalYearMonth.RealYear;

		/// <summary>
		/// 実際の年月を取得します。
		/// </summary>
		public DateTime RealYeaMonth => this.FiscalYearMonth.RealYeaMonth;

		#endregion

		#region メソッド

		/// <summary>
		/// 年月度の開始日と終了日の情報に変換します。
		/// </summary>
		protected virtual void Convert() {
			var day = this.FiscalYearMonth.RealYeaMonth;

			var startDate = this.IsPreviousMonth ? day.AddMonths(-1) : day;
			var endDate = day;

			this.StartDay = new DateTime(startDate.Year, startDate.Month, this.FirstDayOfMonth);
			this.EndDay = this.StartDay.AddMonths(1).AddDays(-1);
		}

		#endregion
	}
}
