﻿using System;
using AmsDatabaseConnectionLibrary.Interfaces;
using AmsDatabaseConnectionLibrary.Model.Primitive;

namespace AmsDatabaseConnectionLibrary.Model.Common {
	/// <summary>
	/// 年月度の情報を格納するモデルクラスです。
	/// </summary>
	public class FiscalYearMonth : ModelBase, IFiscalYearMonth {
		#region コンストラクタ

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="fiscalYear">年度</param>
		/// <param name="fiscalMonth">月度</param>
		public FiscalYearMonth(string fiscalYear, string fiscalMonth) : this(int.Parse(fiscalYear), int.Parse(fiscalMonth)) {
		}

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="fiscalYear">年度</param>
		/// <param name="fiscalMonth">月度</param>
		public FiscalYearMonth(int fiscalYear, int fiscalMonth) {
			this.FiscalYear = fiscalYear;
			this.FiscalMonth = fiscalMonth;
		}

		#endregion

		#region プロパティ

		/// <summary>
		/// 年度を取得します。
		/// </summary>
		public int FiscalYear { get; protected set; }

		/// <summary>
		/// 月度を取得します。
		/// </summary>
		public int FiscalMonth { get; protected set; }

		/// <summary>
		/// 実際の年を取得します。
		/// </summary>
		public int RealYear
			=> this.RealYeaMonth.Year;

		/// <summary>
		/// 実際の年月を取得します。
		/// </summary>
		public DateTime RealYeaMonth
			=> new DateTime(this.FiscalMonth <= 3 ? this.FiscalYear + 1 : this.FiscalYear, this.FiscalMonth, 1);

		#endregion
	}
}
