﻿using System;
using AmsDatabaseConnectionLibrary.Interfaces;
using AmsDatabaseConnectionLibrary.Model.Primitive;
using ExtensionsLibrary.Extensions;

namespace AmsDatabaseConnectionLibrary.Model.Common {
	/// <summary>
	/// 入退室時刻の情報を格納するクラスです。
	/// </summary>
	public class AttendanceInfo : ModelBase, IAttendanceInfo {
		#region コンストラクタ

		/// <summary>
		/// コンストラクタ
		/// </summary>
		public AttendanceInfo() : this(null, null) {
		}

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="inTime">入室時刻文字列</param>
		/// <param name="outTime">退室時刻文字列</param>
		public AttendanceInfo(string inTime, string outTime) {
			this.access_in_time = inTime;
			this.access_out_time = outTime;
		}

		#endregion

		#region プロパティ

		/// <summary>
		/// 入室時刻 (文字列)
		/// </summary>
		public string access_in_time { get; set; }

		/// <summary>
		/// 退室時刻 (文字列)
		/// </summary>
		public string access_out_time { get; set; }

		/// <summary>
		/// 入室時刻 (値)
		/// </summary>
		public TimeSpan? InTime => this.access_in_time.ToTimeSpan();

		/// <summary>
		/// 退室時刻 (値)
		/// </summary>
		public TimeSpan? OutTime => this.access_out_time.ToTimeSpan();

		#endregion
	}
}
