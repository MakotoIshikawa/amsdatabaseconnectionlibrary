﻿using ExtensionsLibrary.Extensions;

namespace AmsDatabaseConnectionLibrary.Enums {
	/// <summary>
	/// 勤怠区分の列挙体です。
	/// </summary>
	public enum AttendanceTypes {
		/// <summary>
		/// 不明
		/// </summary>
		Unknown = 0,

		/// <summary>
		/// 出勤
		/// </summary>
		出勤 = 01,

		/// <summary>
		/// 公休
		/// </summary>
		公休 = 02,

		/// <summary>
		/// 年休
		/// </summary>
		年休 = 03,

		/// <summary>
		/// 特休
		/// </summary>
		特休 = 04,

		/// <summary>
		/// 失年休
		/// </summary>
		失年休 = 05,

		/// <summary>
		/// 産休
		/// </summary>
		産休 = 06,

		/// <summary>
		/// 病欠
		/// </summary>
		病欠 = 07,

		/// <summary>
		/// 私欠
		/// </summary>
		私欠 = 08,

		/// <summary>
		/// 直行
		/// </summary>
		直行 = 21,

		/// <summary>
		/// 遅刻
		/// </summary>
		遅刻 = 22,

		/// <summary>
		/// 直帰
		/// </summary>
		直帰 = 31,

		/// <summary>
		/// 早退
		/// </summary>
		早退 = 32,
	}

	/// <summary>
	/// AttendanceTypes を拡張するメソッドを提供します。
	/// </summary>
	public static class AttendanceTypesExtension {
		#region メソッド

		/// <summary>
		/// 勤怠区分コードの数字コード文字列に変換します。
		/// </summary>
		/// <param name="this">AttendanceDivisionCodeTypes</param>
		/// <returns>勤怠区分コードの数字コード文字列を返します。</returns>
		public static string ToCodeString(this AttendanceTypes @this)
			=> @this.ToInt32().ToString("00");

		#endregion
	}
}
