﻿namespace AmsDatabaseConnectionLibrary.Enums.Swan {
	/// <summary>
	/// 社員区分を表す列挙体です。
	/// </summary>
	/// <remarks>スワン用</remarks>
	public enum EmployeeTypes {
		/// <summary>
		/// 想定外の値
		/// </summary>
		Unknown = 0,

		/// <summary>
		/// マネージ
		/// </summary>
		マネージ = 1,

		/// <summary>
		/// キャリア
		/// </summary>
		キャリア = 2,

		/// <summary>
		/// パート社員
		/// </summary>
		パート社員 = 8,
	}
}
