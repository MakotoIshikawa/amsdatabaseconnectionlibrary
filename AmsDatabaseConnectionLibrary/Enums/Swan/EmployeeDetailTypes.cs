﻿using System;

namespace AmsDatabaseConnectionLibrary.Enums.Swan {
	/// <summary>
	/// 詳細な社員区分を表す列挙体です。
	/// </summary>
	/// <remarks>スワン用</remarks>
	public enum EmployeeDetailTypes {
		/// <summary>
		/// 想定外の値
		/// </summary>
		Unknown = 0,

		/// <summary>
		/// マネージ
		/// </summary>
		マネージ = 1,

		/// <summary>
		/// キャリア
		/// </summary>
		キャリア = 2,

		/// <summary>
		/// マネージ(60歳以上)
		/// </summary>
		マネージ_60age = 11,

		/// <summary>
		/// キャリア(60歳以上)
		/// </summary>
		キャリア_60age = 21,

		/// <summary>
		/// パート社員(3h 勤務)
		/// </summary>
		パート_3h = 83,

		/// <summary>
		/// パート社員(4h 勤務)
		/// </summary>
		パート_4h = 84,

		/// <summary>
		/// パート社員(5h 勤務)
		/// </summary>
		パート_5h = 85,

		/// <summary>
		/// パート社員(6h 勤務)
		/// </summary>
		パート_6h = 86,

		/// <summary>
		/// パート社員(7h 勤務)
		/// </summary>
		パート_7h = 87,
	}

	/// <summary>
	/// EmployeeDetailTypes を拡張するメソッドを提供します。
	/// </summary>
	public static class EmployeeDetailTypesExtension {
		#region メソッド

		/// <summary>
		/// 社員区分に変換します。
		/// </summary>
		/// <returns>変換した社員区分を返します。</returns>
		public static EmployeeTypes ToEmployee(this EmployeeDetailTypes @this) {
			switch (@this) {
			case EmployeeDetailTypes.マネージ:
			case EmployeeDetailTypes.マネージ_60age:
				return EmployeeTypes.マネージ;
			case EmployeeDetailTypes.キャリア:
			case EmployeeDetailTypes.キャリア_60age:
				return EmployeeTypes.キャリア;
			case EmployeeDetailTypes.パート_3h:
			case EmployeeDetailTypes.パート_4h:
			case EmployeeDetailTypes.パート_5h:
			case EmployeeDetailTypes.パート_6h:
			case EmployeeDetailTypes.パート_7h:
				return EmployeeTypes.パート社員;
			default:
				return EmployeeTypes.Unknown;
			}
		}

		#endregion
	}
}
