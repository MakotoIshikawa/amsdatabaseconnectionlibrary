﻿namespace AmsDatabaseConnectionLibrary.Enums.Swan {
	/// <summary>
	/// 権限コードの列挙体です。
	/// </summary>
	/// <remarks>
	/// (株)スワン 用の仕様です。
	/// </remarks>
	public enum AuthorityCodeTypes {
		/// <summary>
		/// 想定外の値
		/// </summary>
		Unknown = 0,

		/// <summary>
		/// 一般
		/// </summary>
		一般 = 1,

		/// <summary>
		/// 副店長
		/// </summary>
		副店長,

		/// <summary>
		/// 店長
		/// </summary>
		店長,

		/// <summary>
		/// 役職者
		/// </summary>
		役職者,

		/// <summary>
		/// システム管理者
		/// </summary>
		システム管理者,
	}
}
