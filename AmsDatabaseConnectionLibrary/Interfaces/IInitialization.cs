﻿namespace AmsDatabaseConnectionLibrary.Interfaces {
	/// <summary>
	/// 初期設定情報を提供するインターフェイスです。
	/// </summary>
	public interface IInitialization {
		#region プロパティ

		/// <summary>
		/// セクション句
		/// </summary>
		string SECTION { get; set; }

		/// <summary>
		/// 名前
		/// </summary>
		string NAME { get; set; }

		/// <summary>
		/// 値
		/// </summary>
		string VALUE { get; set; }

		#endregion
	}
}