﻿using System;

namespace AmsDatabaseConnectionLibrary.Interfaces {
	/// <summary>
	/// ユーザー情報を提供するインターフェイスです。
	/// </summary>
	public interface IUser : IAuthorityCode, IEmployeeDetail {
		#region プロパティ

		/// <summary>
		/// 社員番号
		/// </summary>
		string syain_no { get; set; }

		/// <summary>
		/// 組織ID
		/// </summary>
		int org_id { get; set; }

		/// <summary>
		/// 所属CD
		/// </summary>
		string belong_cd { get; set; }

		/// <summary>
		/// 所属店所名称
		/// </summary>
		string belong_name { get; set; }

		/// <summary>
		/// ユーザーID
		/// </summary>
		string user_id { get; set; }

		/// <summary>
		/// パスワード
		/// </summary>
		string user_pass { get; set; }

		/// <summary>
		/// 氏名
		/// </summary>
		string user_name { get; set; }

		/// <summary>
		/// 氏名（カナ）
		/// </summary>
		string user_name_kana { get; set; }

		/// <summary>
		/// 主担当CD
		/// </summary>
		string main_charge_cd { get; set; }

		/// <summary>
		/// 担当CD
		/// </summary>
		string charge_cd { get; set; }

		/// <summary>
		/// 職務CD
		/// </summary>
		string job_cd { get; set; }

		/// <summary>
		/// 通知用メールアドレス
		/// </summary>
		string notice_mail_adr { get; set; }

		/// <summary>
		/// 勤怠実績送信用メールアドレス
		/// </summary>
		string jisseki_mail_adr { get; set; }

		/// <summary>
		/// 前回ログイン日時
		/// </summary>
		DateTime? last_login_date { get; set; }

		/// <summary>
		/// 今回ログイン日時
		/// </summary>
		DateTime? this_login_date { get; set; }

		/// <summary>
		/// パスワード発行日時
		/// </summary>
		DateTime? password_date { get; set; }

		/// <summary>
		/// 勤務状況表示順
		/// </summary>
		decimal? joukyou_display_order { get; set; }

		/// <summary>
		/// 勤務状況表示フラグ
		/// </summary>
		string joukyou_display_flag { get; set; }

		/// <summary>
		/// 超勤計算区分コード
		/// </summary>
		string overtime_calc_kbn_cd { get; set; }

		/// <summary>
		/// 勤務時間
		/// </summary>
		decimal? work_time { get; set; }

		/// <summary>
		/// 休憩時間
		/// </summary>
		decimal? rest_time { get; set; }

		/// <summary>
		/// カードID
		/// </summary>
		string card_id { get; set; }

		/// <summary>
		/// 日額設定
		/// </summary>
		decimal? daily_amount { get; set; }

		#endregion
	}
}