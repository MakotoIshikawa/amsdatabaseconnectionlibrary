﻿namespace AmsDatabaseConnectionLibrary.Interfaces {
	/// <summary>
	/// 交番カレンダーの共通キーとなるインターフェイスです。
	/// </summary>
	public interface ICalendarInfo : IEmployeeType, IFiscalYear {
	}
}