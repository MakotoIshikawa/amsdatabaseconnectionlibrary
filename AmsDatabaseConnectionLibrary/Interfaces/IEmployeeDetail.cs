﻿using AmsDatabaseConnectionLibrary.Enums.Swan;

namespace AmsDatabaseConnectionLibrary.Interfaces {
	public interface IEmployeeDetail : IEmployeeType {
		/// <summary>
		/// 社員区分詳細
		/// </summary>
		EmployeeDetailTypes EmployeeDetailType { get; }
	}
}