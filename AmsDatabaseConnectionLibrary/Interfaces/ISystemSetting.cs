﻿using System;

namespace AmsDatabaseConnectionLibrary.Interfaces {
	/// <summary>
	/// システム設定を提供するインターフェイスです。
	/// </summary>
	public interface ISystemSetting {
		#region プロパティ

		/// <summary>
		/// セッション保持時間（分）
		/// </summary>
		decimal? session_keep_min { get; set; }

		#region パスワード管理

		/// <summary>
		/// パスワード履歴保持数
		/// </summary>
		decimal? check_pre_pass_cnt { get; set; }

		/// <summary>
		/// パスワード有効日数
		/// </summary>
		decimal? pass_valid_day { get; set; }

		#endregion

		#region メールサーバー

		/// <summary>
		/// 送信メールサーバー名
		/// </summary>
		string send_mailsv_name { get; set; }

		/// <summary>
		/// 受信メールサーバー名
		/// </summary>
		string receive_mailsv_name { get; set; }

		#endregion

		#region 出勤データ受信

		/// <summary>
		/// 出勤データ受信用接続ID
		/// </summary>
		string enter_user_id { get; set; }

		/// <summary>
		/// 出勤データ受信用接続パスワード
		/// </summary>
		string enter_user_pass { get; set; }

		/// <summary>
		/// 出勤データ受信用メールアドレス
		/// </summary>
		string enter_mail_adr { get; set; }

		#endregion

		#region 退勤データ受信

		/// <summary>
		/// 退勤データ受信用接続ID
		/// </summary>
		string leave_user_id { get; set; }

		/// <summary>
		/// 退勤データ受信用接続パスワード
		/// </summary>
		string leave_user_pass { get; set; }

		/// <summary>
		/// 退勤データ受信用メールアドレス
		/// </summary>
		string leave_mail_adr { get; set; }

		#endregion

		#region メール送信

		/// <summary>
		/// メール送信用接続ID
		/// </summary>
		string send_user_id { get; set; }

		/// <summary>
		/// メール送信用接続パスワード
		/// </summary>
		string send_user_pass { get; set; }

		/// <summary>
		/// メール送信用メールアドレス
		/// </summary>
		string send_mail_adr { get; set; }

		#endregion

		#region 取得間隔

		/// <summary>
		/// 入退室履歴取得間隔（分）
		/// </summary>
		decimal? check_file_interval_min { get; set; }

		/// <summary>
		/// 勤怠実績メール取得間隔（分）
		/// </summary>
		decimal? check_mail_interval_min { get; set; }

		#endregion

		/// <summary>
		/// 年度期首日
		/// </summary>
		DateTime beginning_date { get; set; }

		#endregion
	}
}