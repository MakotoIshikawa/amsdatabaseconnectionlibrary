﻿namespace AmsDatabaseConnectionLibrary.Interfaces {
	/// <summary>
	/// 勤怠情報の共通キーとなるインターフェイスです。
	/// </summary>
	public interface IWorkInfo : IFiscalYear {
		/// <summary>
		/// 社員番号
		/// </summary>
		string syain_no { get; set; }
	}
}