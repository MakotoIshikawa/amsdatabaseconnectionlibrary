﻿namespace AmsDatabaseConnectionLibrary.Interfaces {
	/// <summary>
	/// 月度情報の共通キーとなるインターフェイスです。
	/// </summary>
	public interface IFiscalMonth : IFiscalYear {
		/// <summary>
		/// 月度
		/// </summary>
		string getsudo { get; set; }

		/// <summary>
		/// 月度の数値を取得します。
		/// </summary>
		int GetsudoVal { get; }
	}
}