﻿using AmsDatabaseConnectionLibrary.Enums;

namespace AmsDatabaseConnectionLibrary.Interfaces {
	/// <summary>
	/// 勤怠区分の共通キーとなるインターフェイスです。
	/// </summary>
	public interface IAttendanceCategory {
		/// <summary>
		/// 勤怠区分
		/// </summary>
		string kintai_kbn_cd { get; set; }

		/// <summary>
		/// 勤怠区分を取得します。
		/// </summary>
		AttendanceTypes AttendanceType { get; }
	}
}