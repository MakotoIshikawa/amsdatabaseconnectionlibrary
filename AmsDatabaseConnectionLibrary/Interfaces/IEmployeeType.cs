﻿using AmsDatabaseConnectionLibrary.Enums.Swan;

namespace AmsDatabaseConnectionLibrary.Interfaces {
	/// <summary>
	/// 社員区分情報の共通キーとなるインターフェイスです。
	/// </summary>
	public interface IEmployeeType {
		/// <summary>
		/// 社員区分コード
		/// </summary>
		string syain_kbn_cd { get; set; }

		/// <summary>
		/// 社員区分
		/// </summary>
		EmployeeTypes EmployeeType { get; }
	}
}