﻿namespace AmsDatabaseConnectionLibrary.Interfaces {
	/// <summary>
	/// 年度情報の共通キーとなるインターフェイスです。
	/// </summary>
	public interface IFiscalYear {
		/// <summary>
		/// 年度
		/// </summary>
		string nendo { get; set; }

		/// <summary>
		/// 年度の数値を取得します。
		/// </summary>
		int NendoVal { get; }
	}
}