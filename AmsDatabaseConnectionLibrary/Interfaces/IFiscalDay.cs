﻿using System;

namespace AmsDatabaseConnectionLibrary.Interfaces {
	/// <summary>
	/// 勤務日毎情報の共通キーとなるインターフェイスです。
	/// </summary>
	public interface IFiscalDay : IFiscalMonth, IAttendanceCategory {
		/// <summary>
		/// 勤務日
		/// </summary>
		DateTime work_day { get; set; }
	}
}