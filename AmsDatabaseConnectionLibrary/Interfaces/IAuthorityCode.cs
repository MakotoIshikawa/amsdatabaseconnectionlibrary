﻿using AmsDatabaseConnectionLibrary.Enums.Swan;

namespace AmsDatabaseConnectionLibrary.Interfaces {
	/// <summary>
	/// 権限コードの共通キーとなるインターフェイスです。
	/// </summary>
	public interface IAuthorityCode {
		/// <summary>
		/// 権限コード
		/// </summary>
		string kengen_cd { get; set; }

		/// <summary>
		/// 権限コードを取得します。
		/// </summary>
		AuthorityCodeTypes AuthorityCode { get; }
	}
}