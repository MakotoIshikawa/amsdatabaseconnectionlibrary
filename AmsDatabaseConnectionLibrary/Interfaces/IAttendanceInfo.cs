﻿using System;

namespace AmsDatabaseConnectionLibrary.Interfaces {
	/// <summary>
	/// 入退室時刻の情報を提供するインターフェイスです。
	/// </summary>
	public interface IAttendanceInfo {
		#region プロパティ

		/// <summary>
		/// 入室時刻 (文字列)
		/// </summary>
		string access_in_time { get; set; }

		/// <summary>
		/// 退室時刻 (文字列)
		/// </summary>
		string access_out_time { get; set; }

		/// <summary>
		/// 入室時刻 (値)
		/// </summary>
		TimeSpan? InTime { get; }

		/// <summary>
		/// 退室時刻 (値)
		/// </summary>
		TimeSpan? OutTime { get; }

		#endregion
	}
}