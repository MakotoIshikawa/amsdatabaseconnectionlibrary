﻿using System;

namespace AmsDatabaseConnectionLibrary.Interfaces {
	/// <summary>
	/// DBレコードの履歴情報を提供するインターフェイスです。
	/// </summary>
	public interface IHistoryInfo {
		#region プロパティ

		/// <summary>
		/// 登録日時
		/// </summary>
		DateTime ins_date { get; set; }

		/// <summary>
		/// 登録者ID
		/// </summary>
		string ins_syain_no { get; set; }

		/// <summary>
		/// 更新日時
		/// </summary>
		DateTime upd_date { get; set; }

		/// <summary>
		/// 更新者ID
		/// </summary>
		string upd_syain_no { get; set; }

		/// <summary>
		/// 削除日時
		/// </summary>
		DateTime? del_date { get; set; }

		/// <summary>
		/// 削除者ID
		/// </summary>
		string del_syain_no { get; set; }

		#endregion
	}

	/// <summary>
	/// IHistoryInfo を継承するクラスを拡張するメソッドを提供します。
	/// </summary>
	public static partial class HistoryInfoExtension {
		#region メソッド

		/// <summary>
		/// 論理削除されたレコードかどうかを取得します。
		/// </summary>
		/// <param name="this">IHistoryInfo</param>
		/// <returns>論理削除されたレコードの場合は、true を返します。</returns>
		public static bool GetIsDeleted(this IHistoryInfo @this)
			=> @this.del_date.HasValue;

		#endregion
	}
}