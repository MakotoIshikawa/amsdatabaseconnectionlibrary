﻿using System;

namespace AmsDatabaseConnectionLibrary.Interfaces {
	/// <summary>
	/// 年月度の情報を提供するインターフェイスです。
	/// </summary>
	public interface IFiscalYearMonth {
		#region プロパティ

		/// <summary>
		/// 年度を取得します。
		/// </summary>
		int FiscalYear { get; }

		/// <summary>
		/// 月度を取得します。
		/// </summary>
		int FiscalMonth { get; }

		/// <summary>
		/// 実際の年を取得します。
		/// </summary>
		int RealYear { get; }

		/// <summary>
		/// 実際の年月を取得します。
		/// </summary>
		DateTime RealYeaMonth { get; }

		#endregion
	}
}