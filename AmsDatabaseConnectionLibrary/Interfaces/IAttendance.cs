﻿using System;

namespace AmsDatabaseConnectionLibrary.Interfaces {
	/// <summary>
	/// 出退勤情報を提供するインターフェイスです。
	/// </summary>
	public interface IAttendance {
		#region プロパティ

		/// <summary>
		/// ユーザーID
		/// </summary>
		string USER_ID { get; set; }
		/// <summary>
		/// カード番号
		/// </summary>
		string CARD_NUM { get; set; }

		/// <summary>
		/// ユーザー名
		/// </summary>
		string USER_NAME { get; set; }

		/// <summary>
		/// 日付
		/// </summary>
		DateTime WORK_DATE { get; set; }

		/// <summary>
		/// 出勤時刻
		/// </summary>
		DateTime ENTRY_TIME { get; set; }

		/// <summary>
		/// 退勤時刻
		/// </summary>
		DateTime? EXIT_TIME { get; set; }

		#endregion
	}
}