using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using AmsDatabaseConnectionLibrary.Enums;
using AmsDatabaseConnectionLibrary.Enums.Swan;
using AmsDatabaseConnectionLibrary.Interfaces;
using ExtensionsLibrary.Extensions;

namespace AmsDatabaseConnectionLibrary {
	/// <summary>
	/// LINQ to SQL で勤怠管理システムのデータにアクセスするためのクラスです。
	/// </summary>
	public partial class AmsDataClassesDataContext {
		#region ユーザーマスタ

		#region ユーザー情報取得

		/// <summary>
		/// 登録されているユーザー情報のコレクションを取得します。
		/// </summary>
		/// <returns>ユーザー情報のコレクションを返します。</returns>
		public IQueryable<m_user> GetEnrolledUsers()
			=> this.m_user.GetValidItems();

		/// <summary>
		/// ユーザーID を指定して
		/// 登録されているユーザー情報を取得します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <returns>ユーザー情報を返します。</returns>
		/// <remarks>該当するユーザー情報が取得できない場合は、null を返します。</remarks>
		public m_user GetEnrolledUser(string id)
			=> this.GetEnrolledUsers().SingleOrDefault(u => u.syain_no == id);

		/// <summary>
		/// カード番号を指定して
		/// 登録されているユーザー情報を取得します。
		/// </summary>
		/// <param name="card">カード番号</param>
		/// <returns>ユーザー情報を返します。</returns>
		public m_user GetUserByCard(string card)
			=> this.GetEnrolledUsers().SingleOrDefault(u => u.card_id == card);

		/// <summary>
		/// ログインユーザー ID を指定して
		/// ログインしたユーザーの情報を取得します。
		/// </summary>
		/// <param name="userId">ログインユーザー ID</param>
		/// <returns>ユーザー情報を返します。</returns>
		/// <remarks>該当するユーザー情報が取得できない場合は、null を返します。</remarks>
		public m_user GetLoginUser(string userId)
			=> this.GetEnrolledUsers().SingleOrDefault(u => u.user_id == userId);

		#endregion

		#region ユーザー情報削除

		/// <summary>
		/// 指定されたユーザーを論理削除します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="editorId">更新者ID</param>
		public void DeleteAtLogicalOfUser(string id, string editorId) {
			this.m_user.DeleteAtLogical(editorId, users => users.Where(u => u.syain_no == id));
			this.SubmitChanges();
		}

		#endregion

		/// <summary>
		/// 指定された論理削除されているユーザーを復旧します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="editorId">更新者ID</param>
		public void RecoverOfUser(string id, string editorId) {
			this.m_user.Recover(editorId, users => users.Where(u => u.syain_no == id));
			this.SubmitChanges();
		}

		/// <summary>
		/// 社員区分と年度と月度を指定して、
		/// 計画労働日数を取得します。
		/// </summary>
		/// <param name="employee">社員区分</param>
		/// <param name="fiscalYear">年度</param>
		/// <param name="fiscalMonth">月度</param>
		/// <returns>計画労働日数を返します。</returns>
		public int GetWorkPlanDaysTotal(EmployeeTypes employee, int fiscalYear, int fiscalMonth) {
			var kbn = employee.ToInt32().ToString();
			var nen = $"{fiscalYear:0000}";
			var getu = $"{fiscalMonth:00}";
			var query = (
				from d in this.m_base_calendar_d
				join k in this.m_kintai_kbn
				  on d.kintai_kbn_cd equals k.kintai_kbn_cd
				where
				  d.syain_kbn_cd == kbn
				  && d.nendo == nen
				  && d.getsudo == getu
				select new {
					d.kintai_kbn_cd,
					k.work_evaluation_flg
				}
			);

			var cnt = query.Count(i => i.work_evaluation_flg == "1");
			return cnt;
		}

		/// <summary>
		/// ユーザーIDと年度と月度を指定して、
		/// 労働日数を取得します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="fiscalYear">年度</param>
		/// <param name="fiscalMonth">月度</param>
		/// <returns>労働日数を返します。</returns>
		public int GetWorkDaysTotal(string id, int fiscalYear, int fiscalMonth) {
			var nen = $"{fiscalYear:0000}";
			var getu = $"{fiscalMonth:00}";
			var query = (
				from u in this.GetEnrolledUsers()
				join d in this.t_work_d
				  on u.syain_no equals d.syain_no
				join k in this.m_kintai_kbn
				  on d.kintai_kbn_cd equals k.kintai_kbn_cd
				where
				  u.syain_no == id
				  && d.nendo == nen
				  && d.getsudo == getu
				select new {
					u.user_name,
					d.kintai_kbn_cd,
					k.work_evaluation_flg
				}
			);

			var cnt = query.Count(i => i.work_evaluation_flg == "1");
			return cnt;
		}

		/// <summary>
		/// ユーザーIDと年度と月度を指定して、
		/// 月額を取得します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="fiscalYear">年度</param>
		/// <param name="fiscalMonth">月度</param>
		/// <returns>月額を返します。</returns>
		public decimal? GetMonthlyAmount(string id, int fiscalYear, int fiscalMonth) {
			var cnt = this.GetWorkDaysTotal(id, fiscalYear, fiscalMonth);
			var user = this.GetEnrolledUser(id);
			var sum = user.daily_amount * cnt;
			return sum;
		}

		/// <summary>
		/// ユーザーIDと年度と月度を指定して、
		/// 計画勤務時間の合計を取得します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="fiscalYear">年度</param>
		/// <param name="fiscalMonth">月度</param>
		/// <returns>計画勤務時間の合計値を返します。</returns>
		public TimeSpan GetMonthlyWorkPlanTimes(string id, int fiscalYear, int fiscalMonth) {
			var user = this.GetEnrolledUser(id);
			var cnt = this.GetWorkPlanDaysTotal(user.EmployeeType, fiscalYear, fiscalMonth);
			var sum = Convert.ToDouble(user.work_time * cnt);
			return TimeSpan.FromHours(sum);
		}

		/// <summary>
		/// ユーザーID を指定して
		/// ユーザーのログイン日時を更新します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		public void UpDateLastLoginDate(string id) {
			var now = DateTime.Now;
			var user = this.GetEnrolledUser(id);
			{
				user.upd_date = now;
				user.upd_syain_no = id;
				user.last_login_date = user.this_login_date;
				user.this_login_date = now;
			}

			this.SubmitChanges();
		}

		#region パスワード変更

		/// <summary>
		/// 社員番号とパスワードを指定して、
		/// ユーザーパスワードを変更します。
		/// </summary>
		/// <param name="id">社員番号</param>
		/// <param name="password">パスワード</param>
		public void ChangePassword(string id, string password) {
			var now = DateTime.Now;

			var user = this.GetEnrolledUser(id);
			user.upd_date = now;
			user.upd_syain_no = id;

			// パスワード更新
			user.user_pass = password;
			user.password_date = now;

			// 新しいパスワードを履歴に残す
			this.m_user_pass_history.InsertOnSubmit(new m_user_pass_history() {
				syain_no = id,
				ins_date = now,
				ins_syain_no = id,
				upd_date = now,
				upd_syain_no = id,
				user_pass = password
			});

			this.SubmitChanges();
		}

		#endregion

		#endregion

		#region ユーザー休暇マスタ

		#endregion

		#region 初期設定マスタ

		/// <summary>
		/// セクション名を指定して設定情報のコレクションを取得します。
		/// </summary>
		/// <param name="section">セクション名</param>
		/// <returns>設定情報のコレクションを返します。</returns>
		public Dictionary<string, string> GetConfig(string section)
			=> (
				from i in this.MASTER_INITIALIZATION
				where i.SECTION == section
				select i
			).ToDictionary(i => i.NAME.Trim(), i => i.VALUE.Trim());

		/// <summary>
		/// 初期設定情報を取得します。
		/// </summary>
		/// <returns>初期設定情報を返します。</returns>
		public TSystem GetSystemSettings<TSystem>() where TSystem : class, ISystemSetting, new() {
			var section = "AMS";
			var dic = this.GetConfig(section);
			var sys = new TSystem();
			{
				sys.session_keep_min = dic.GetValueOrDefault(nameof(sys.session_keep_min)).ToNullableDecimal();
				sys.check_pre_pass_cnt = dic.GetValueOrDefault(nameof(sys.check_pre_pass_cnt)).ToNullableDecimal();
				sys.pass_valid_day = dic.GetValueOrDefault(nameof(sys.pass_valid_day)).ToNullableDecimal();

				sys.send_mailsv_name = dic.GetValueOrDefault(nameof(sys.send_mailsv_name));
				sys.receive_mailsv_name = dic.GetValueOrDefault(nameof(sys.receive_mailsv_name));

				sys.enter_user_id = dic.GetValueOrDefault(nameof(sys.enter_user_id));
				sys.enter_user_pass = dic.GetValueOrDefault(nameof(sys.enter_user_pass));
				sys.enter_mail_adr = dic.GetValueOrDefault(nameof(sys.enter_mail_adr));

				sys.leave_user_id = dic.GetValueOrDefault(nameof(sys.leave_user_id));
				sys.leave_user_pass = dic.GetValueOrDefault(nameof(sys.leave_user_pass));
				sys.leave_mail_adr = dic.GetValueOrDefault(nameof(sys.leave_mail_adr));

				sys.send_user_id = dic.GetValueOrDefault(nameof(sys.send_user_id));
				sys.send_user_pass = dic.GetValueOrDefault(nameof(sys.send_user_pass));
				sys.send_mail_adr = dic.GetValueOrDefault(nameof(sys.send_mail_adr));

				sys.check_file_interval_min = dic.GetValueOrDefault(nameof(sys.check_file_interval_min)).ToNullableDecimal();
				sys.check_mail_interval_min = dic.GetValueOrDefault(nameof(sys.check_mail_interval_min)).ToNullableDecimal();
				sys.beginning_date = DateTime.Parse(dic.GetValueOrDefault(nameof(sys.beginning_date), "2018/03/16"));
			}

			return sys;
		}

		/// <summary>
		/// 初期設定情報を更新します。
		/// </summary>
		/// <param name="data">更新データ</param>
		public void UpdateSystemSettings(ISystemSetting data) {
#if true
			var dic = data.ToPropertyDictionary();
			var items = (
				from p in dic
				where p.Value != null
				select new MASTER_INITIALIZATION() {
					SECTION = "AMS",
					NAME = p.Key,
					VALUE = p.Value.ToString()
				}
			);

			items.ForEach(item => {
				var target = this.MASTER_INITIALIZATION.SingleOrDefault(v => v.SECTION == item.SECTION & v.NAME == item.NAME);
				if (target == null) {
					this.MASTER_INITIALIZATION.InsertOnSubmit(item);
				} else {
					var withBlock = target;
					withBlock.VALUE = item.VALUE;
				}
			});

			this.SubmitChanges();
#else
			var system = this.m_system.FirstOrDefault();
			{
				system.session_keep_min = data.session_keep_min;
				system.check_pre_pass_cnt = data.check_pre_pass_cnt;
				system.pass_valid_day = data.pass_valid_day;
				system.send_mailsv_name = data.send_mailsv_name;
				system.receive_mailsv_name = data.receive_mailsv_name;
				system.enter_user_id = data.enter_user_id;
				system.enter_user_pass = data.enter_user_pass;
				system.enter_mail_adr = data.enter_mail_adr;
				system.leave_user_id = data.leave_user_id;
				system.leave_user_pass = data.leave_user_pass;
				system.leave_mail_adr = data.leave_mail_adr;
				system.send_user_id = data.send_user_id;
				system.send_user_pass = data.send_user_pass;
				system.send_mail_adr = data.send_mail_adr;
				system.check_file_interval_min = data.check_file_interval_min;
				system.check_mail_interval_min = data.check_mail_interval_min;
				system.beginning_date = data.beginning_date;
			}

			this.SubmitChanges();
#endif
		}

		#endregion

		#region 日次勤怠情報

		/// <summary>
		/// 日次勤怠情報の労働時間を更新します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="year">年度</param>
		/// <param name="month">月度</param>
		/// <param name="authorizer">承認者</param>
		public void UpdateWorkTime(string id, int year, int month, string authorizer) {
			var items = this.t_work_d.Where(d => d.syain_no == id & d.nendo == year.ToString() & d.getsudo == month.ToString("00"));

			foreach (var item in items) {
				var modified = false;

				item.PropertyChanged += (sender, e) => {
					modified = true;
				};

				var inTime = item.jisseki_in_time.ToTimeSpan() ?? item.InTime;
				var outTime = item.jisseki_out_time.ToTimeSpan() ?? item.OutTime;
				var restTime = item.jisseki_rest_time.ToTimeSpan() ?? item.kouban_rest_time.ToTimeSpan();
				var workTime = outTime - inTime;

				if (!workTime.HasValue) {
					continue;
				}

				if (workTime < TimeSpan.Zero) {
					throw new InvalidOperationException($"{item.work_day.ToShortDateString()}:入室時刻({inTime})が退室時刻({outTime})よりも後に設定されています。");
				}

				workTime -= restTime;
				item.work_time = workTime?.ToHourAndMinString();

				if (modified) {
					item.upd_date = DateTime.Now;
					item.upd_syain_no = authorizer;
				}
			}

			this.SubmitChanges();
		}

		#endregion
	}

	/// <summary>
	/// AmsDataClassesDataContext を拡張するメソッドを提供します。
	/// </summary>
	public static partial class AmsDataClassesDataContextExtension {
		#region メソッド

		/// <summary>
		/// 論理削除されていない有効なアイテムのみを取得します。
		/// </summary>
		/// <typeparam name="T">IHistoryInfo</typeparam>
		/// <param name="this">Table</param>
		/// <returns>有効なアイテムのコレクションを返します。</returns>
		public static IQueryable<T> GetValidItems<T>(this Table<T> @this) where T : class, IHistoryInfo
			=> @this.Where(x => !x.del_date.HasValue);

		#region DeleteAtLogical

		/// <summary>
		/// 論理削除します。
		/// </summary>
		/// <typeparam name="T">IHistoryInfo</typeparam>
		/// <param name="this">Table</param>
		/// <param name="editorId">更新者ID</param>
		/// <param name="func">論理削除するアイテムコレクションを取得するメソッド</param>
		public static void DeleteAtLogical<T>(this Table<T> @this, string editorId, Func<Table<T>, IQueryable<T>> func) where T : class, IHistoryInfo
			=> @this.DeleteAtLogical(editorId, DateTime.Now, func);

		/// <summary>
		/// 論理削除します。
		/// </summary>
		/// <typeparam name="T">IHistoryInfo</typeparam>
		/// <param name="this">Table</param>
		/// <param name="editorId">更新者ID</param>
		/// <param name="modified">更新日時</param>
		/// <param name="func">論理削除するアイテムコレクションを取得するメソッド</param>
		public static void DeleteAtLogical<T>(this Table<T> @this, string editorId, DateTime modified, Func<Table<T>, IQueryable<T>> func) where T : class, IHistoryInfo {
			var items = func?.Invoke(@this);
			items?.ToList()?.ForEach(item => {
				item.upd_date = modified;
				item.upd_syain_no = editorId;
				item.del_date = modified;
				item.del_syain_no = editorId;
			});
		}

		#endregion

		#region Recover

		/// <summary>
		/// 論理削除されているアイテムを復旧します。
		/// </summary>
		/// <typeparam name="T">IHistoryInfo</typeparam>
		/// <param name="this">Table</param>
		/// <param name="editorId">更新者ID</param>
		/// <param name="func">復旧させるアイテムコレクションを取得するメソッド</param>
		public static void Recover<T>(this Table<T> @this, string editorId, Func<Table<T>, IQueryable<T>> func) where T : class, IHistoryInfo
			=> @this.Recover(editorId, DateTime.Now, func);

		/// <summary>
		/// 論理削除されているアイテムを復旧します。
		/// </summary>
		/// <typeparam name="T">IHistoryInfo</typeparam>
		/// <param name="this">Table</param>
		/// <param name="editorId">更新者ID</param>
		/// <param name="modified">更新日時</param>
		/// <param name="func">復旧させるアイテムコレクションを取得するメソッド</param>
		public static void Recover<T>(this Table<T> @this, string editorId, DateTime modified, Func<Table<T>, IQueryable<T>> func) where T : class, IHistoryInfo {
			var items = func?.Invoke(@this);
			items?.ToList()?.ForEach(item => {
				item.upd_date = modified;
				item.upd_syain_no = editorId;
				item.del_date = null;
				item.del_syain_no = null;
			});
		}

		#endregion

		#endregion
	}

	/// <summary>
	/// M_日次交番カレンダー
	/// </summary>
	public partial class m_base_calendar_d : IHistoryInfo, ICalendarInfo, IFiscalDay {
		#region プロパティ

		/// <summary>
		/// 年度の数値を取得します。
		/// </summary>
		public int NendoVal => Convert.ToInt32(this.nendo);

		/// <summary>
		/// 月度の数値を取得します。
		/// </summary>
		public int GetsudoVal => Convert.ToInt32(this.getsudo);

		/// <summary>
		/// 社員区分を取得します。
		/// </summary>
		public EmployeeTypes EmployeeType => this.syain_kbn_cd.ToEnum<EmployeeTypes>(EmployeeTypes.Unknown);

		/// <summary>
		/// 勤怠区分を取得します。
		/// </summary>
		public AttendanceTypes AttendanceType => this.kintai_kbn_cd.ToEnum<AttendanceTypes>(AttendanceTypes.Unknown);

		#endregion

		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// M_月次交番カレンダー
	/// </summary>
	public partial class m_base_calendar_m : IHistoryInfo, ICalendarInfo, IFiscalMonth {
		#region プロパティ

		/// <summary>
		/// 年度の数値を取得します。
		/// </summary>
		public int NendoVal => Convert.ToInt32(this.nendo);

		/// <summary>
		/// 月度の数値を取得します。
		/// </summary>
		public int GetsudoVal => Convert.ToInt32(this.getsudo);

		/// <summary>
		/// 社員区分を取得します。
		/// </summary>
		public EmployeeTypes EmployeeType => this.syain_kbn_cd.ToEnum<EmployeeTypes>(EmployeeTypes.Unknown);

		#endregion

		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// M_月次交番カレンダー
	/// </summary>
	public partial class m_base_calendar_y : IHistoryInfo, ICalendarInfo {
		#region プロパティ

		/// <summary>
		/// 年度の数値を取得します。
		/// </summary>
		public int NendoVal => Convert.ToInt32(this.nendo);

		/// <summary>
		/// 社員区分を取得します。
		/// </summary>
		public EmployeeTypes EmployeeType => this.syain_kbn_cd.ToEnum<EmployeeTypes>(EmployeeTypes.Unknown);

		#endregion

		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// M_権限
	/// </summary>
	public partial class m_kengen : IHistoryInfo, IAuthorityCode {
		#region プロパティ

		/// <summary>
		/// 権限コードを取得します。
		/// </summary>
		public AuthorityCodeTypes AuthorityCode => this.kengen_cd.ToEnum<AuthorityCodeTypes>(AuthorityCodeTypes.Unknown);

		#endregion

		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// M_勤怠区分
	/// </summary>
	public partial class m_kintai_kbn : IHistoryInfo, IAttendanceCategory {
		#region プロパティ

		/// <summary>
		/// 勤怠区分を取得します。
		/// </summary>
		public AttendanceTypes AttendanceType
			=> this.kintai_kbn_cd.ToEnum<AttendanceTypes>(AttendanceTypes.Unknown);

		#endregion

		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// M_社員区分
	/// </summary>
	public partial class m_syain_kbn : IHistoryInfo, IEmployeeType {
		#region プロパティ

		/// <summary>
		/// 社員区分を取得します。
		/// </summary>
		public EmployeeTypes EmployeeType => this.EmployeeDetailType.ToEmployee();

		/// <summary>
		/// 社員区分詳細を取得します。
		/// </summary>
		public EmployeeDetailTypes EmployeeDetailType => this.syain_kbn_cd.ToEnum<EmployeeDetailTypes>(EmployeeDetailTypes.Unknown);

		#endregion

		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// M_システム設定
	/// </summary>
	[Obsolete("汎用的ではない為廃止予定。MASTER_INITIALIZATION クラスを使用します。")]
	public partial class m_system : IHistoryInfo, ISystemSetting {
		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// M_組織
	/// </summary>
	public partial class m_org : IHistoryInfo {
		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// M_ユーザー
	/// </summary>
	public partial class m_user : IHistoryInfo, IUser {
		#region プロパティ

		/// <summary>
		/// 社員区分を取得します。
		/// </summary>
		public EmployeeTypes EmployeeType => this.EmployeeDetailType.ToEmployee();

		/// <summary>
		/// 社員区分詳細を取得します。
		/// </summary>
		public EmployeeDetailTypes EmployeeDetailType => this.syain_kbn_cd.ToEnum<EmployeeDetailTypes>(EmployeeDetailTypes.Unknown);

		/// <summary>
		/// 権限コードを取得します。
		/// </summary>
		public AuthorityCodeTypes AuthorityCode => this.kengen_cd.ToEnum<AuthorityCodeTypes>(AuthorityCodeTypes.Unknown);

		/// <summary>
		/// 勤務時間を取得します。
		/// </summary>
		public TimeSpan? WorkTime => this.work_time.ToNullable(t => TimeSpan.FromHours(Convert.ToDouble(t)));

		/// <summary>
		/// 休憩時間を取得します。
		/// </summary>
		public TimeSpan? RestTime => this.rest_time.ToNullable(t => TimeSpan.FromHours(Convert.ToDouble(t)));

		#endregion

		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// M_ユーザー休暇
	/// </summary>
	public partial class m_user_holiday : IHistoryInfo {
		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// M_ユーザーパスワード履歴
	/// </summary>
	public partial class m_user_pass_history : IHistoryInfo {
		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// T_メール受信履歴
	/// </summary>
	[Obsolete("スワン様のシステムでは使用しません。")]
	public partial class t_mail_receive_history : IHistoryInfo {
		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// T_入退室履歴
	/// </summary>
	[Obsolete("スワン様のシステムでは使用しません。ATTENDANCE クラスを使用します。")]
	public partial class t_room_access_history : IHistoryInfo {
		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// T_日次勤怠情報
	/// </summary>
	public partial class t_work_d : IHistoryInfo, IWorkInfo, IFiscalDay, IAttendanceInfo {
		#region プロパティ

		/// <summary>
		/// 年度の数値を取得します。
		/// </summary>
		public int NendoVal => Convert.ToInt32(this.nendo);

		/// <summary>
		/// 月度の数値を取得します。
		/// </summary>
		public int GetsudoVal => Convert.ToInt32(this.getsudo);

		/// <summary>
		/// 勤怠区分を取得します。
		/// </summary>
		public AttendanceTypes AttendanceType => this.kintai_kbn_cd.ToEnum<AttendanceTypes>(AttendanceTypes.Unknown);

		/// <summary>
		/// 入室時刻 (値)
		/// </summary>
		public TimeSpan? InTime => this.access_in_time.ToTimeSpan();

		/// <summary>
		/// 退室時刻 (値)
		/// </summary>
		public TimeSpan? OutTime => this.access_out_time.ToTimeSpan();

		#endregion

		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// T_月次勤怠情報
	/// </summary>
	public partial class t_work_m : IHistoryInfo, IWorkInfo, IFiscalMonth {
		#region プロパティ

		/// <summary>
		/// 年度の数値を取得します。
		/// </summary>
		public int NendoVal => Convert.ToInt32(this.nendo);

		/// <summary>
		/// 月度の数値を取得します。
		/// </summary>
		public int GetsudoVal => Convert.ToInt32(this.getsudo);

		#endregion

		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// T_年次勤怠情報
	/// </summary>
	public partial class t_work_y : IHistoryInfo, IWorkInfo {
		#region プロパティ

		/// <summary>
		/// 年度の数値を取得します。
		/// </summary>
		public int NendoVal => Convert.ToInt32(this.nendo);

		#endregion

		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// 出退勤情報
	/// </summary>
	public partial class ATTENDANCE : IAttendance {
		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}

	/// <summary>
	/// 初期設定マスター
	/// </summary>
	public partial class MASTER_INITIALIZATION : IInitialization {
		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。(オーバーライド)
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> this.GetPropertiesString();

		#endregion
	}
}